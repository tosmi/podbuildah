#!/bin/bash

set -euf -o pipefail

SUDO=/usr/bin/sudo
RELEASE=30


install_chrome() {
  sudo dnf -y install fedora-workstation-repositories
  sudo dnf config-manager --set-enabled google-chrome
  sudo dnf makecache
  sudo rpm -e flatpak-xdg-utils || true
  sudo dnf -y install google-chrome-stable pulseaudio pulseaudio-utils
}

install_chrome
