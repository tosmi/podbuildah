#!/bin/bash

set -euf -o pipefail

SUDO=/usr/bin/sudo
RELEASE=35

PKGS="emacs texlive ImageMagick autoconf make gcc gcc-c++ perl-TimeDate automake openssl-devel zlib-devel cyrus-sasl-devel glib2-devel libpng-devel poppler-devel poppler-glib-devel pinentry-gnome3 gmime xapian-core html2text libtool gmime-devel xapian-core-devel texinfo gmime30-devel golang  openssh-server file ansible-core ansible-lint firefox jq aspell aspell-en aspell-de ShellCheck rlwrap rpm-build fuse-sshfs cups-client gstreamer1-plugin-openh264 mozilla-openh264 graphviz isync ripgrep the_silver_searcher texlive-german texlive-currvita texlive-wrapfig npm cmake tmux plantuml bind-utils pandoc redhat-text-fonts redhat-display-fonts rubygem-asciidoctor rubygem-asciidoctor-pdf rubygem-rouge sqlite evince tigervnc sassc inkscape texlive-ulem texlive-capt-of xdg-utils openssh-askpass calibre gperf zeal passwdqc pass ripgrep pass-otp"


RPMFUSION_FREE="https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-${RELEASE}.noarch.rpm"
RPMFUSION_NONFREE="https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-${RELEASE}.noarch.rpm"

install_clojure() {
    # XXX this sucks but who cares....
    curl https://download.clojure.org/install/linux-install-1.10.1.469.sh | sudo bash
    curl https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein > $HOME/bin/lein
    chmod 755 $HOME/bin/lein
}

enable_rpmfusion() {
    if ! $SUDO dnf -y install "$RPMFUSION_FREE"; then
	echo "Failed to instal rpm fusion free repo"
	return 1
    fi

    if ! $SUDO dnf -y install "$RPMFUSION_NONFREE"; then
	echo "Failed to instal rpm fusion non-free repo"
	return 1
    fi
}

enable_fedora_h264() {
    if ! sudo dnf config-manager --set-enabled fedora-cisco-openh264; then
        echo "Could not enable openh264 repo!"
        return 1
    fi
}

exit_if_failed() {
    if [ "$?" != 0 ]; then
	exit 1
    fi
}

install_pkgs() {
    enable_rpmfusion
    exit_if_failed

    enable_fedora_h264
    exit_if_failed

   grep -q exclude /etc/dnf/dnf.conf || $SUDO sh -c 'echo "exclude=rpm-plugin-systemd-inhibit" >> /etc/dnf/dnf.conf'


    if ! $SUDO dnf -y install $PKGS; then
	echo "Failed to install required packages!"
	return 1
    fi
}

install_azure() {
    sudo sh -c 'echo -e "[azure-cli]
name=Azure CLI
baseurl=https://packages.microsoft.com/yumrepos/azure-cli
enabled=1
gpgcheck=1
gpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/azure-cli.repo'

    sudo yum install -y azure-cli
}

setup_golang() {
  go get -u golang.org/x/tools/cmd/...
  go get -u github.com/rogpeppe/godef/...
  go get -u github.com/nsf/gocode
  go get -u golang.org/x/tools/cmd/goimports
  go get -u golang.org/x/tools/cmd/guru
  go get -u github.com/dougm/goflymake
}

setup_ca_trust() {
    sudo cp /home/pinhead/Downloads/EGB-Standard-Web-FullChain.crt /etc/pki/ca-trust/source/anchors/
    sudo update-ca-trust
}

setup_pyenv() {
    git clone https://github.com/pyenv/pyenv.git ~/.pyenv

    # these package are required for building python
    PYTHON_PKGS="readline-devel sqlite-devel"
    if ! $SUDO dnf -y install $PYTHONE_ PKGS; then
	echo "Failed to install required python packages!"
	return 1
    fi
}

sudo yum update -y

sudo rpm --quiet -qi flatpak-xdg-utils && sudo rpm -e flatpak-xdg-utils

install_pkgs
exit_if_failed

install_clojure
install_azure

setup_golang
# setup_ca_trust
