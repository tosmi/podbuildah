FC_VERSION := 40

.PHONY: image
image:
	buildah build --build-arg=FC_VERSION=$(FC_VERSION) --env=VERSION=$(FC_VERSION) -t toolbox-$(FC_VERSION) container/

.PHONY: create
create: image
	toolbox create -i localhost/toolbox-$(FC_VERSION):latest -c home-$(FC_VERSION)

.PHONY: all
all: image create
