#!/bin/bash

FEDORA_RELEASE=29
CONTAINER_NAME=pinhead-golang

BASE_PACKAGES="passwd sudo"

CONTAINER_PACKAGES="emacs"

dbus_system_bus_path=/var/run/dbus/system_bus_socket

# pick up these env vars from the current environment
#
# HOME=/home/pinhead
# UID=1000
# SHELL=/bin/bash
# USER=pinhead
# XDG_RUNTIME_DIR=/run/user/1000

function build_container() {

    if ! buildah from --name pinhead-golang docker.io/library/fedora:"${FEDORA_RELEASE}"; then
	echo "$0: could not create container"
	return 1
    fi

    if ! buildah config --author "Toni Schmidbauer <toni@stderr.at>" pinhead-golang ; then
	echo "$0: could not create container"
	return 1
    fi

    if ! buildah copy --chown root:root pinhead-golang files/sudo_wheel /etc/sudoers.d/10_wheel; then
	echo "$0: could deploy sudoers for wheel group"
	return 1
    fi


    for package in $BASE_PACKAGES; do
    	if ! buildah run $CONTAINER_NAME -- dnf -y install "$package"; then
    	    echo "$0: installing ${package} failed!"
    	    return 1
    	fi
    done
}

function configure_container() {
    if ! buildah run $CONTAINER_NAME -- useradd \
                 --home-dir $HOME \
                 --no-create-home \
                 --shell $SHELL \
                 --uid $UID \
                 --groups wheel \
                 $USER \
                 ; then
        echo "$0: failed to create user $USER with UID $UID"
        return 1
    fi

    # if ! buildah run $CONTAINER_NAME -- chown -R $USER:$USER /home/pinhead

    if ! buildah run $CONTAINER_NAME -- passwd -d $USER ; then
        echo "$0: failed to remove password for user $USER"
        return 1
    fi

    if ! buildah run $CONTAINER_NAME -- passwd -d root; then
        echo "$0: failed to remove password for user root"
        return 1
    fi

    if ! buildah config --volume $HOME $CONTAINER_NAME ; then
        echo "$0: failed to configure volume for $HOME"
        return 1
    fi

    if ! buildah config --volume $XDG_RUNTIME_DIR $CONTAINER_NAME ; then
        echo "$0: failed to configure volume for $XDG_RUNTIME_DIR"
        return 1
    fi

    if ! buildah config --volume /var/run/dbus/system_bus_socket $CONTAINER_NAME ; then
        echo "$0: failed to configure volume for $dbus_system_bus_path"
        return 1
    fi

    if ! buildah config --volume /dev/dri $CONTAINER_NAME; then
        echo "$0: failed to configure volume for /dev/dri"
        return 1
    fi

    if ! buildah config --volume /dev/fuse $CONTAINER_NAME; then
        echo "$0: failed to configure volume for /dev/fuse"
        return 1
    fi

    if ! buildah config --user $USER $CONTAINER_NAME; then
        echo "$0: failed to configure the default user as $USER"
        return 1
    fi

    if ! buildah config --user $USER $CONTAINER_NAME; then
        echo "$0: failed to configure the default user as $USER"
        return 1
    fi

    if ! buildah config --workingdir $HOME $CONTAINER_NAME; then
        echo "$0: failed to configure the initial working directory to $HOME"
        return 1
    fi

    if ! buildah commit $CONTAINER_NAME $CONTAINER_NAME; then
        echo "$0: failed to configure the initial working directory to $HOME"
        return 1
    fi

}

function install_packages() {
    for package in $CONTAINER_PACKAGES; do
	if ! buildah run $CONTAINER_NAME -- dnf -y install "$package"; then
	    echo "$0: installing ${package} failed!"
	    return 1
	fi
    done
}

function create() {
    max_uid_count=65536
    max_minus_uid=$((max_uid_count - UID))
    uid_plus_one=$((UID + 1))
    tmpfs_size=$((64 * 1024 * 1024)) # 64 MiB

    podman create \
           --group-add wheel \
           --hostname toolbox \
           --interactive \
           --name $CONTAINER_NAME \
           --network host \
           --privileged \
           --security-opt label=disable \
           --tmpfs /dev/shm:size=$tmpfs_size \
           --uidmap $UID:0:1 \
           --uidmap 0:1:$UID \
           --uidmap $uid_plus_one:$uid_plus_one:$max_minus_uid \
           --tty \
           --volume $HOME:$HOME:rslave \
           --volume $XDG_RUNTIME_DIR:$XDG_RUNTIME_DIR \
           --volume $dbus_system_bus_path:$dbus_system_bus_path \
           --volume /dev/dri:/dev/dri \
           --volume /dev/fuse:/dev/fuse \
           $CONTAINER_NAME \
           /bin/sh >/dev/null
}

function enter() {
    shell_to_exec=/bin/bash

    podman exec \
           --env COLORTERM=$COLORTERM \
           --env DBUS_SESSION_BUS_ADDRESS=$DBUS_SESSION_BUS_ADDRESS \
           $set_dbus_system_bus_address \
           --env DESKTOP_SESSION=$DESKTOP_SESSION \
           --env DISPLAY=$DISPLAY \
           --env LANG=$LANG \
           --env SHELL=$SHELL \
           --env SSH_AUTH_SOCK=$SSH_AUTH_SOCK \
           --env TERM=$TERM \
           --env VTE_VERSION=$VTE_VERSION \
           --env XDG_CURRENT_DESKTOP=$XDG_CURRENT_DESKTOP \
           --env XDG_DATA_DIRS=$XDG_DATA_DIRS \
           --env XDG_MENU_PREFIX=$XDG_MENU_PREFIX \
           --env XDG_RUNTIME_DIR=$XDG_RUNTIME_DIR \
           --env XDG_SEAT=$XDG_SEAT \
           --env XDG_SESSION_DESKTOP=$XDG_SESSION_DESKTOP \
           --env XDG_SESSION_ID=$XDG_SESSION_ID \
           --env XDG_SESSION_TYPE=$XDG_SESSION_TYPE \
           --env XDG_VTNR=$XDG_VTNR \
           --interactive \
           --tty \
           $CONTAINER_NAME \
           capsh --caps="" -- -c 'cd "$1"; export PS1="$2"; shift 2; exec "$@"' \
               /bin/sh \
               "$PWD" \
               "$toolbox_prompt" \
               $shell_to_exec -l
}

#build_container
#install_packages
#configure_container

#create
enter
